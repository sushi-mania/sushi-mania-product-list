import { number, string } from 'prop-types';

export default {
  imageUrl: string,
  title: string,
  description: string,
  price: number,
  reference: string,
};
