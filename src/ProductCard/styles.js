import styled from 'styled-components';

export const ProductCardWrapper = styled.div({
  display: 'flex',
  flexDirection: 'column',
  width: 300,
});

export const ProductCardImageWrapper = styled.div({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  height: 300,
});

export const ProductCardImage = styled.img({
  width: 300,
  maxHeight: 300,
});

export const ProductCardActions = styled.div({
  alignItems: 'center',
  display: 'flex',
  justifyContent: 'space-around',
  padding: '0 20px',
});

export const ProductCardContent = styled.div({
  padding: '0 20px',
});
