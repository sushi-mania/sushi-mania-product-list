import { func } from 'prop-types';
import React, { Component } from 'react';

import { Button, Selector } from '@sushi-mania/ui';
import {
  ProductCardWrapper,
  ProductCardImageWrapper,
  ProductCardImage,
  ProductCardActions,
  ProductCardContent,
} from './styles';
import types from './types';

class ProductCard extends Component {
  handleAddButtonClick = () => {
    const { reference, updateProductQuantity } = this.props;

    updateProductQuantity(reference, 1);
  };

  handleQuantitySelectorChange = event => {
    if (event && event.target && event.target.value) {
      const selectedQuantity = parseInt(event.target.value, 10);

      this.setState(() => ({
        selectedQuantity,
      }));
    }
  };

  renderQuantitySelector() {
    const { price } = this.props;
    const availableQuantities = [1, 2, 3, 4, 5, 10];
    const options = availableQuantities.map(quantity => ({
      label: `${quantity} - ${(quantity * price).toFixed(2)} €`,
      value: quantity.toString(),
    }));

    return <Selector value={1} onChange={this.handleQuantitySelectorChange} options={options} />;
  }

  render() {
    const { description, imageUrl, price, title } = this.props;

    if (!price) return null;

    return (
      <ProductCardWrapper>
        <ProductCardImageWrapper>
          <ProductCardImage src={imageUrl} alt={title} />
        </ProductCardImageWrapper>
        <ProductCardActions>
          {this.renderQuantitySelector()}
          <Button action={this.handleAddButtonClick}>Ajouter</Button>
        </ProductCardActions>
        <ProductCardContent>
          <h2>{title}</h2>
          <p>{description}</p>
        </ProductCardContent>
      </ProductCardWrapper>
    );
  }
}

ProductCard.defaultProps = {
  imageUrl: '',
  title: '',
  description: '',
  price: 0,
  reference: '',
  updateProductQuantity: Function.prototype,
};

ProductCard.propTypes = {
  ...types,
  updateProductQuantity: func,
};

export default ProductCard;

export { default as types } from './types';
