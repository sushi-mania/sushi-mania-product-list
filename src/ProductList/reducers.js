import { FETCH_PRODUCTS } from './actions';

const initialState = [];

export const productReducers = (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_PRODUCTS:
      return payload.products;

    default:
      return state;
  }
};
