export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const fetchProducts = products => ({
  type: FETCH_PRODUCTS,
  payload: {
    products,
  },
});
