import styled from 'styled-components';

export const ProductListWrapper = styled.div({
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center',
});
