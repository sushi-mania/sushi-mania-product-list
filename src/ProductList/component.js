import { arrayOf, shape } from 'prop-types';
import React, { Component } from 'react';

import { ProductListWrapper } from './styles';
import ProductCard, { types } from '../ProductCard';

class ProductList extends Component {
  componentDidMount() {
    const { fetchProducts } = this.props;

    fetch('https://sdiscount-api.herokuapp.com/products')
      .then(response => response.json())
      .then(data => {
        const products = data.map(product => ({ ...product, quantity: 0 }));

        fetchProducts(products);
      });
  }

  render() {
    const { products } = this.props;

    return (
      <ProductListWrapper>
        {products.map(({ reference, ...otherProps }) => (
          <ProductCard key={reference} reference={reference} {...otherProps} />
        ))}
      </ProductListWrapper>
    );
  }
}

ProductList.defaultProps = {
  products: [],
};

ProductList.propTypes = {
  products: arrayOf(shape(types)),
};

export default ProductList;
