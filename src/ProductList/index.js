import { connect } from 'react-redux';

import { fetchProducts } from './actions';
import ProductList from './component';

const mapStateToProps = state => ({
  products: state.products,
});

const mapDispatchToProps = {
  fetchProducts,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductList);
